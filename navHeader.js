const navHeader = `<div class="header-container w-100 fixed-style">

<div class="container">

    <div class="header">

        <div class="hamburguer d-flex d-xl-none"><input type="checkbox" class="openSidebarMenu" id="openSidebarMenu"> <label for="openSidebarMenu" class="sidebarIconToggle">

                <div class="spinner diagonal part-1"></div>

                <div class="spinner horizontal"></div>

                <div class="spinner diagonal part-2"></div>

            </label>

            <div id="sidebarMenu" class="hamburguer d-inline d-xl-none">

                <ul class="sidebarMenuInner">

                    <div class="logo"><a href="/"><img alt="logo-emblue" src="../images/emblue-logo-footer.svg"></a></div>

                    <div class="menu-platform-hamburguer">

                        <div style="visibility: hidden; position: absolute; width: 0px; height: 0px;">

                            <svg xmlns="http://www.w3.org/2000/svg">

                                <symbol viewBox="0 0 24 24" id="expand-more">

                                    <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" />

                                    <path d="M0 0h24v24H0z" fill="none" />

                                </symbol>

                                <symbol viewBox="0 0 24 24" id="close">

                                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />

                                    <path d="M0 0h24v24H0z" fill="none" />

                                </symbol>

                            </svg>

                        </div>

                        <details>

                            <summary>

                                Plataforma e Serviços

                                <svg class="control-icon control-icon-expand" width="24" height="24" role="presentation">

                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#expand-more" /></svg>

                                <svg class="control-icon control-icon-close" width="24" height="24" role="presentation">

                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close" /></svg>

                            </summary>

                            <p><a href="automation.html">Automação de Marketing</a><br><br>

                                <a href="integrations.html">Integração</a><br><br>

                                <a href="email-marketing-and-web-connect.html">Email Marketing e Web Connect</a><br><br>

                                <a href="sms-push-notifications.html">Sms e Notificação Push</a><br><br>

                                <a href="remarketing.html">Remarketing</a><br><br>

                                <a href="drag-and-drop.html">Editor Arrasta e Solta (Drag&Drop)</a><br><br>

                                <a href="onsite.html">OnSite</a>

                            </p>

                        </details>

                    </div>

                    <a href="planos.html">

                        <li class="border-top-h">Planos</li>

                    </a>

                    <a href="sobre-emblue-email-marketing.html">

                        <li>Porque emBlue</li>

                    </a>

                    <a href="casos-de-sucesso.html">

                        <li>Casos de Sucesso</li>

                    </a>

                    <a href="cultura.html">

                        <li>Cultura</li>

                    </a>

                    <a href="eventos.html">

                        <li>Eventos</li>

                    </a>

                    <a target="_blank" href="https://app.embluemail.com">

                        <li>Entrar</li>

                    </a>

                    <a class="btn-cta" href="planos.html">Teste a emBlue</a>

                </ul>

            </div>

        </div>

        <div class="logo"><a href="https://www.embluemail.com/pt/ "><img alt="logo-emblue" src="../images/emblue-logo.svg"></a></div>

        <ul class="nav d-none d-xl-flex">

            <!-- novo menu estruturado -->

            <li class="nav-item"><a class="nav-link" title="Quem somos" href="#">Quem somos</a>
                <ul class="about_us-ul_rck200 platform-menu">
                    <!-- Catégoria -->
                    <li class=""><a title="Porque emBlue" href="sobre-emblue-email-marketing.html">Por que emBlue?</a></li>
                    <li class=""><a title="Casos de Sucesso" href="casos-de-sucesso.html">Casos de Sucesso</a></li>
                    <li class=""><a title="Casos de Sucesso" href="servicos.html">Serviços</a></li>
                </ul>
            </li>

            <!-- Departamento -->
            <li class="nav-item"><a class="nav-link" title="Plataforma" href="#">Plataforma e Serviços</a>

                <ul class="plat-serv-ul_rck200 platform-menu">
                    <!-- Catégoria -->

                    <li class=""><a title="conversação" href="#">Conversação</a>
                        <!-- sub-categoria -->
                        <ul class="sub-cat_rck200">
                            <li class=""><a href="email-marketing-and-web-connect.html">Email Marketing</a></li>
                            <li class=""><a href="sms-push-notifications.html">SMS e Notificação Push</a></li>
                            <li class=""><a href="whatsapp-marketing.html">WhatsApp</a></li>
                            <li class=""><a href="onsite.html">OnSite Banners Pop Ups Personalizados</a></li>
                            <li class=""><a href="contact-frequency-scoring.html">Contact Frequency Scoring</a></li>
                        </ul>    
                    </li>

                    <li class=""><a title="Personalização" href="#">Personalização</a>
                        <!-- sub-categoria -->
                        <ul class="sub-cat_rck200">
                            <li class=""><a href="automation.html">Automação de Marketing</a></li>
                            <li class=""><a href="drag-and-drop.html">Editor Drag and Drop - Arrasta e Solta</a></li>
                            <li class=""><a href="net-promoter-score-nps/">Net Promoter Score (NPS) - Avaliações</a></li>
                            <li class=""><a href="remarketing.html">Remarketing</a></li>
                        </ul>    
                    </li>

                    <li class=""><a href="integrations.html">Integração</a></li>     
                </ul>

            </li>

            <li class="nav-item"><a class="nav-link" title="Planes" href="planos.html">Planos</a></li>

            <li class="nav-item"><a class="nav-link" title="Cultura" href="cultura.html">Cultura</a></li>

            <li class="nav-item"><a class="nav-link" title="Blog" href="https://blog.embluemail.com/">Blog</a></li>

            <li class="nav-item pr-7"><a class="nav-link" title="Eventos" href="eventos.html">Eventos</a></li>

            <li class="nav-item"><a href="planos.html" class="btn-demo uppercase" title="Teste a emBlue">Teste a emBlue</a></li>

            <li class="nav-item"><a target="_blank" class="btn-login uppercase mx-3" title="Login" href="https://app.embluemail.com">Entrar →</a></li>

        </ul>

        <div class="languajes">

            <p><a href="https://www.embluemail.com/"><b>ES </b></a>-

                <a href="https://www.embluemail.com/en/"><b>EN</b></a></p>

        </div>

    </div>

    <div class="separator"></div>

</div>

</div>`

let myBody = document.querySelector('body');

myBody.insertAdjacentHTML('afterbegin', navHeader);
